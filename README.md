After you download or clone the repo do the following:
Running the Angular App:
    - Install NodeJS and NPM from https://nodejs.org.
    - Install the Angular CLI using the command: npm install -g @angular/cli
    - Start the app with the Angular CLI command ng serve --open this will compile the Angular app and automatically launch it in the browser on the URL http://localhost:4500.

Running the ASP.NET Core 5 API:
    - Install the .NET Core SDK from https://www.microsoft.com/net/download/core.
    - Make sure the instance of MSSQL is already installed on your machine (MSSQL 2017 were used to implement the task)
    - Open the project in visual studio and configure your connection string in (appsettings.Development.json, appsettings.Production.json)
    - Run the WebApi project, you should see the message "Application Starting"
    - Make sure which environment variable you are using: 
        http://localhost:4010; https://localhost:4011; Development environment
        http://localhost:5010;https://localhost:5011; Production environment


Notes:
    - Please make sure the mentions ports are available