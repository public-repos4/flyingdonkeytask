﻿using Core.Application.Behaviours;
using Core.Application.Features.Files.Queries.GetFiles;
using Core.Application.Helpers;
using Core.Application.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Core.Application
{
	public static class ServiceExtensions
	{
		public static void AddApplicationLayer(this IServiceCollection services)
		{
			services.AddAutoMapper(Assembly.GetExecutingAssembly());
			services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
			services.AddMediatR(Assembly.GetExecutingAssembly());
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));

			services.AddScoped<IDataShapeHelper<GetFileDto>, DataShapeHelper<GetFileDto>>();

			services.AddScoped<IModelHelper, ModelHelper>();
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
		}
	}
}