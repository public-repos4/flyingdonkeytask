﻿using Core.Application.Features.Files.Queries.GetFiles;
using Core.Application.Parameters;
using Core.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Interfaces.Repositories
{
    public interface IFileRepositoryAsync : IGenericRepositoryAsync<File>
    {
        Task<(IEnumerable<File> data, RecordsCount recordsCount)> GetPagedFileResponseAsync(GetFilesQuery queryParams);
    }
}