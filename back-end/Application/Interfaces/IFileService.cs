﻿using Core.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Core.Application.Interfaces
{
	public interface IFileService
	{
		string Upload(IFormFile File, string relativeUploadPath);
		void Remove(string relativeRemovePath);
	}
}