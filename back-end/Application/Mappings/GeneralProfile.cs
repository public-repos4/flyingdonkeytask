﻿using AutoMapper;
using Core.Application.Features.Files.Queries.GetFiles;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Core.Application.Mappings
{
	public class GeneralProfile : Profile
	{
		public GeneralProfile()
		{
			CreateMap<File, GetFileDto>()
				.ForMember(dest => dest.UploadDate, opt => opt.MapFrom(src => src.CreatedAt.ToString("yyyy-MM-dd")))
				.AfterMap<RefactoreFileLink>();
		}

		public class BaseMappingAction
		{
			private readonly IHttpContextAccessor _httpContextAccessor;

			public BaseMappingAction(IHttpContextAccessor httpContextAccessor)
			{
				_httpContextAccessor = httpContextAccessor;
			}

			public string GetServerPath()
			{
				return _httpContextAccessor.HttpContext.Request.IsHttps
				   ? "https://" + _httpContextAccessor.HttpContext.Request.Host.ToString()
				   : "http://" + _httpContextAccessor.HttpContext.Request.Host.ToString();
			}

			public string GetCulture()
			{
				List<string> allowdCultures = new() { "en", "ar" };

				IHeaderDictionary headers = _httpContextAccessor.HttpContext.Request.Headers;

				string culture = "en";

				if (headers.ContainsKey("Accept-Language"))
				{
					if (allowdCultures.Contains(headers["Accept-Language"].ToString()))
					{
						culture = headers["Accept-Language"].ToString();
					}
				}

				return culture;
			}
		}

		public class RefactoreFileLink : BaseMappingAction, IMappingAction<File, GetFileDto>
		{
			public RefactoreFileLink(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor) { }

			public void Process(File source, GetFileDto destination, ResolutionContext context)
			{
				string ServerPath = GetServerPath();
				destination.Link = $"{ServerPath}{source.Path}";
			}
		}
	}
}