﻿
namespace Core.Application.Parameters.Datatable
{
    public class Search
    {
        public string Value { get; set; }
        public bool Regex { get; set; }
    }
}