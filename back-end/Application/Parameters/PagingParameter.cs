﻿
namespace Core.Application.Parameters
{
    public class PagingParameter
    {
        private const int maxPageSize = 10000;
        private int? _pageSize = 50;
        private int? _pageNumber = 1;

        public int? PageNumber  // If null => don't do the  pagination and get all records
        {
            get => _pageNumber;
            set => _pageNumber = (value < 1 && value != -1) ? 1 : value; // -1 represent the last page : invalid number
        }

        public int? PageSize    // If null => don't do the  pagination and get all records
        {
            get => _pageSize;
            set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
        }
    }
}