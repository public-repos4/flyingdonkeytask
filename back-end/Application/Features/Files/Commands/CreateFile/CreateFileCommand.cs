﻿using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace Core.Application.Features.Files.Commands.CreateFile
{
	public partial class CreateFileCommand : IRequest<Response<long>>
	{
		public IFormFile File { get; set; }
	}

	public class CreateFileCommandHandler : IRequestHandler<CreateFileCommand, Response<long>>
	{
		private readonly IFileRepositoryAsync _fileRepository;
		private readonly IFileService _fileService;
		private readonly IConfiguration _config;

		public CreateFileCommandHandler(
			IFileRepositoryAsync fileRepository,
			IFileService fileService,
			IConfiguration config)
		{
			_fileRepository = fileRepository;
			_fileService = fileService;
			_config = config;
		}

		public async Task<Response<long>> Handle(CreateFileCommand command, CancellationToken cancellationToken)
		{
			string[] sizes = { "B", "KB", "MB", "GB", "TB" };
			double len = command.File.Length;
			int order = 0;
			while (len >= 1024 && order < sizes.Length - 1)
			{
				order++;
				len /= 1024;
			}

			// Adjust the format string to your preferences. For example "{0:0.#}{1}" would
			// show a single decimal place, and no space.
			string fileSize = String.Format("{0:0.##} {1}", len, sizes[order]);

			var relativeUploadPath = _config.GetSection("DefaultUploadPath").Value;
			string fileFullFilePath = _fileService.Upload(command.File, relativeUploadPath);

			File file = new()
			{
				OriginName = System.IO.Path.GetFileNameWithoutExtension(command.File.FileName),
				Path = fileFullFilePath,
				Name = fileFullFilePath.Split('/').Last().Split('.').First(),
				Extension = System.IO.Path.GetExtension(command.File.FileName),
				Size = fileSize
			};

			await _fileRepository.AddAsync(file);
			return new Response<long>(file.Id, "SucceededCreate");
		}
	}
}