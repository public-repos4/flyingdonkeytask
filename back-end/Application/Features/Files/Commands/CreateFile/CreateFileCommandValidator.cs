﻿using FluentValidation;
using System.Linq;

namespace Core.Application.Features.Files.Commands.CreateFile
{
    public class CreateFileCommandValidator : AbstractValidator<CreateFileCommand>
    {
        public CreateFileCommandValidator()
        {
            RuleFor(x => x.File)
                .NotNull().WithMessage("File is required");

            RuleFor(x => x.File.FileName)
                .Must(HaveSupportedFileType).WithMessage("png, gif, jpeg, jpg, txt, pdf only are allowed")
                .When(x => x.File != null);

            RuleFor(x => x.File.Length).ExclusiveBetween(1, 2 * 1024 * 1024) // 2 MB
                .WithMessage("Maximum 2 MB file size allowed")
                .When(x => x.File != null);
        }

        private bool HaveSupportedFileType(string fileName)
        {
            string[] allowedFileTypes = { ".png", ".gif", ".jpeg", ".jpg", ".txt", ".pdf" };
            return allowedFileTypes.Contains(System.IO.Path.GetExtension(fileName));
        }
    }
}