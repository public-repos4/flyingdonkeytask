﻿using Core.Application.Exceptions;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Files.Commands.DeleteFileById
{
	public class DeleteFileByIdCommand : IRequest<Response<long>>
	{
		public long Id { get; set; }

		public class DeleteFileByIdCommandHandler : IRequestHandler<DeleteFileByIdCommand, Response<long>>
		{
			private readonly IFileRepositoryAsync _fileRepository;
			private readonly IFileService _fileService;

			public DeleteFileByIdCommandHandler(
				IFileRepositoryAsync fileRepository,
				IFileService fileService)
			{
				_fileRepository = fileRepository;
				_fileService = fileService;
			}

			public async Task<Response<long>> Handle(DeleteFileByIdCommand command, CancellationToken cancellationToken)
			{
				var file = await _fileRepository.GetByIdAsync(command.Id);
				if (file == null) throw new ApiException($"File Not Found.");

				_fileService.Remove(file.Path);

				await _fileRepository.DeleteAsync(file);
				return new Response<long>(file.Id, "SucceededDelete");
			}
		}
	}
}