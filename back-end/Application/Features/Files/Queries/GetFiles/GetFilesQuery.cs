﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Files.Queries.GetFiles
{
	public class GetFilesQuery : QueryParameter, IRequest<PagedResponse<IEnumerable<Entity>>>
	{
		public string OriginName { get; set; }
		public string Path { get; set; }
		public string Name { get; set; }
		public string Extension { get; set; }
		public string Size { get; set; }
	}

	public class GetAllFilesQueryHandler : IRequestHandler<GetFilesQuery, PagedResponse<IEnumerable<Entity>>>
	{
		private readonly IFileRepositoryAsync _fileRepository;
		private readonly IModelHelper _modelHelper;
		private readonly IDataShapeHelper<GetFileDto> _dataShaper;
		private readonly IMapper _mapper;

		public GetAllFilesQueryHandler(
			IFileRepositoryAsync fileRepository, 
			IModelHelper modelHelper,
			IMapper mapper,
			IDataShapeHelper<GetFileDto> dataShaper)
		{
			_fileRepository = fileRepository;
			_modelHelper = modelHelper;
			_dataShaper = dataShaper;
			_mapper = mapper;
		}

		public async Task<PagedResponse<IEnumerable<Entity>>> Handle(GetFilesQuery queryParams, CancellationToken cancellationToken)
		{
			var validQueryParams = queryParams;

			//filtered fields security
			if (!string.IsNullOrEmpty(validQueryParams.Fields))
			{
				//limit to fields in view model
				validQueryParams.Fields = _modelHelper.ValidateModelFields<GetFileDto>(validQueryParams.Fields);
			}
			else
			{
				//default fields from view model
				validQueryParams.Fields = _modelHelper.GetModelFields<GetFileDto>();
			}

			// query based on filter
			var result = await _fileRepository.GetPagedFileResponseAsync(validQueryParams);
			var data = _mapper.Map<IEnumerable<GetFileDto>>(result.data);
			RecordsCount recordCount = result.recordsCount;

			var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

			// response wrapper
			return new PagedResponse<IEnumerable<Entity>>(shapeData, validQueryParams.PageNumber, validQueryParams.PageSize, recordCount);
		}
	}
}