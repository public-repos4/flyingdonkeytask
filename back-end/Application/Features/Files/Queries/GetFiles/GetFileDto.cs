﻿using System;

namespace Core.Application.Features.Files.Queries.GetFiles
{
    public class GetFileDto
    {
        public long Id { get; set; }
        public string OriginName { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Size { get; set; }
        public string Link { get; set; }
        public string UploadDate { get; set; }
    }
}