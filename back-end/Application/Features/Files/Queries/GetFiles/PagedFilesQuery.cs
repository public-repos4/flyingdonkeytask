﻿using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Application.Parameters.Datatable;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Files.Queries.GetFiles
{
    public partial class PagedFilesQuery : IRequest<PagedDataTableResponse<IEnumerable<Entity>>>
    {
        //strong type input parameters
        public int Draw { get; set; } //page number
        public int Start { get; set; } //Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the first record).
        public int Length { get; set; } //page size
        public IList<Order> Order { get; set; } //Order by
        public Search Search { get; set; } //search criteria
        public IList<Column> Columns { get; set; } //select fields
    }

    public class PageFileQueryHandler : IRequestHandler<PagedFilesQuery, PagedDataTableResponse<IEnumerable<Entity>>>
    {
        private readonly IFileRepositoryAsync _fileRepository;
        private readonly IModelHelper _modelHelper;
        private readonly IDataShapeHelper<GetFileDto> _dataShaper;
        private readonly IMapper _mapper;

        public PageFileQueryHandler(
            IFileRepositoryAsync fileRepository, 
            IModelHelper modelHelper,
            IMapper mapper,
            IDataShapeHelper<GetFileDto> dataShaper)
        {
            _fileRepository = fileRepository;
            _modelHelper = modelHelper;
            _dataShaper = dataShaper;
            _mapper = mapper;
        }

        public async Task<PagedDataTableResponse<IEnumerable<Entity>>> Handle(PagedFilesQuery dtQueryParams, CancellationToken cancellationToken)
        {
			var validQueryParams = new GetFilesQuery
			{
				// Draw map to PageNumber
				PageNumber = (dtQueryParams.Start / dtQueryParams.Length) + 1,
				// Length map to PageSize
				PageSize = dtQueryParams.Length
			};

			// Map order > OrderBy
			var colOrder = dtQueryParams.Order[0];
            switch (colOrder.Column)
            {
                case 0:
                    validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Id" : "Id DESC";
                    break;

                case 1:
                    validQueryParams.OrderBy = colOrder.Dir == "asc" ? "ArabicName" : "ArabicName DESC";
                    break;

                case 2:
                    validQueryParams.OrderBy = colOrder.Dir == "asc" ? "EnglishName" : "EnglishName DESC";
                    break;

                case 3:
                    validQueryParams.OrderBy = colOrder.Dir == "asc" ? "Location" : "Location DESC";
                    break;
            }

            // Map Search > searchable columns
            if (!string.IsNullOrEmpty(dtQueryParams.Search.Value))
            {
                //limit to fields in view model
                validQueryParams.OriginName = dtQueryParams.Search.Value;
                validQueryParams.Path = dtQueryParams.Search.Value;
                validQueryParams.Name = dtQueryParams.Search.Value;
                validQueryParams.Extension = dtQueryParams.Search.Value;
                validQueryParams.Size = dtQueryParams.Search.Value;
            }

            if (string.IsNullOrEmpty(validQueryParams.Fields))
            {
                //default fields from view model
                validQueryParams.Fields = _modelHelper.GetModelFields<GetFileDto>();
            }

            // query based on filter
            var result = await _fileRepository.GetPagedFileResponseAsync(validQueryParams);
            var data = _mapper.Map<IEnumerable<GetFileDto>>(result.data);
            RecordsCount recordCount = result.recordsCount;

            var shapeData = _dataShaper.ShapeData(data, validQueryParams.Fields);

            // response wrapper
            return new PagedDataTableResponse<IEnumerable<Entity>>(shapeData, dtQueryParams.Draw, recordCount);
        }
    }
}