﻿using Core.Application.Exceptions;
using Core.Application.Interfaces.Repositories;
using Core.Application.Wrappers;
using Core.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Files.Queries.GetFileById
{
    public class GetFileByIdQuery : IRequest<Response<File>>
    {
        public long Id { get; set; }

        public class GetFileByIdQueryHandler : IRequestHandler<GetFileByIdQuery, Response<File>>
        {
            private readonly IFileRepositoryAsync _fileRepository;

            public GetFileByIdQueryHandler(IFileRepositoryAsync fileRepository)
            {
                _fileRepository = fileRepository;
            }

            public async Task<Response<File>> Handle(GetFileByIdQuery queryParams, CancellationToken cancellationToken)
            {
                var file = await _fileRepository.GetByIdAsync(queryParams.Id);
                if (file == null) throw new ApiException($"File Not Found.");
                return new Response<File>(file);
            }
        }
    }
}