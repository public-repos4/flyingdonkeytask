﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Domain.Common
{
    public abstract class BaseEntity
    {
        public virtual long Id { get; set; }

        //public DateTime CreatedAt { get; set; }    //Null in case of update
        //public DateTime? LastModifiedAt { get; set; }    //Null in case of create 
        ////public DateTime? DeletedAt { get; set; }

        //public long CreatedBy { get; set; }    //Null in case of update
        //public long? LastModifiedBy { get; set; }    //Null in case of create
    }
}