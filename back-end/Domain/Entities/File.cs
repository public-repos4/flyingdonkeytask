﻿using Core.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Domain.Entities
{
    public class File : AuditableBaseEntity
    {
        public string OriginName { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Size { get; set; }
    }   
}