﻿using Core.Application.Interfaces;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Infrastructure.Shared.Services
{
	public class FileService : IFileService
	{
		public string Upload(IFormFile file, string relativeUploadPath)
		{
			string baseWebApiUrl = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") switch
			{
				// Local server
				"Development" => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/bin" }, StringSplitOptions.None)[0],

				// Online server
				_ => new Uri(Assembly.GetExecutingAssembly().CodeBase)
					.AbsolutePath.Split(new string[] { "/Shared.dll" }, StringSplitOptions.None)[0],
			};

			string fullUploadFolderPath = Path.Combine(
			baseWebApiUrl +
			"/wwwroot" +
			relativeUploadPath);

			fullUploadFolderPath = fullUploadFolderPath.Replace("/", @"\");
			fullUploadFolderPath = fullUploadFolderPath.Replace("%20", " ");

			// Upload to this folder after creating it (if not exists)
			Directory.CreateDirectory(fullUploadFolderPath);

			string newFileName = Guid.NewGuid().ToString();

			string fullFilePath = Path.Combine(
				fullUploadFolderPath +
				newFileName +
				Path.GetExtension(file.FileName));

			fullFilePath = fullFilePath.Replace("/", @"\");
			fullFilePath = fullFilePath.Replace("%20", " ");

			using (var fileStreams = new FileStream(fullFilePath, FileMode.Create))
			{
				file.CopyTo(fileStreams);
			}

			return Path.Combine(relativeUploadPath + newFileName + Path.GetExtension(file.FileName));

		}

		public void Remove(string relativeRemovePath)
		{
			// fullRemovePath (remove folder or file)
			string fullRemovePath = Path.Combine(
				new Uri(Assembly.GetExecutingAssembly().CodeBase)
				.AbsolutePath.Split(new string[] { "/bin" }, StringSplitOptions.None)[0] +
				"/wwwroot" +
				relativeRemovePath);

			fullRemovePath = fullRemovePath.Replace("/", @"\");
			fullRemovePath = fullRemovePath.Replace("%20", " ");

			if (System.IO.File.Exists(fullRemovePath))
			{
				System.IO.File.Delete(fullRemovePath);

			}
			else if (System.IO.Directory.Exists(fullRemovePath))
			{
				Directory.Delete(fullRemovePath, true);
			}
		}
	}
}