﻿using Core.Application.Features.Files.Commands.CreateFile;
using Core.Application.Features.Files.Commands.DeleteFileById;
using Core.Application.Features.Files.Queries.GetFiles;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Presentation.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class FilesController : BaseApiController
    {
        /// <summary>
        /// POST: api/v1/[controller]
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
		public async Task<IActionResult> Post([FromForm] CreateFileCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// GET: api/[controller]
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetFilesQuery queryParams)
        {
            return Ok(await Mediator.Send(queryParams));
        }

        /// <summary>
        /// DELETE: api/v1/[controller]/{id}
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            return Ok(await Mediator.Send(new DeleteFileByIdCommand { Id = id }));
        }
    }
}