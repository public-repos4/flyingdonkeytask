﻿using Infrastructure.Persistence.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Presentation.WebApi.Extensions
{
	public static class ServiceExtensions
	{
		public static void AddSwaggerExtension(this IServiceCollection services)
		{
			services.AddSwaggerGen(c =>
			{
				c.IncludeXmlComments(XmlCommentsFilePath);
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Version = "v1",
					Title = "Flying Donkey task - v1",
					Description = "This Api will be responsible for overall data distribution and authorization.",
					Contact = new OpenApiContact
					{
						Name = "Wajeeh Abiad",
						Email = "wajeeh.abiad@gmail.com",
						Url = new Uri("http://wajeehabiad.com/#contact"),
					}
				});
			});
		}

		public static void AddControllersExtension(this IServiceCollection services)
		{
			services.AddControllers()
				.AddNewtonsoftJson(options =>
				{
					options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				});
		}


		// CORS
		//Configure CORS to allow any origin, header and method. 
		//Change the CORS policy based on your requirements.
		//More info see: https://docs.microsoft.com/en-us/aspnet/core/security/cors?view=aspnetcore-3.0
		public static void AddCorsExtension(this IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy("AllowAll", builder =>
				{
					builder.AllowAnyOrigin()
						   .AllowAnyHeader()
						   .AllowAnyMethod();
				});
			});
		}


		// API version
		public static void AddApiVersioningExtension(this IServiceCollection services)
		{
			services.AddApiVersioning(config =>
			{
				// Specify the default API Version as 1.0
				config.DefaultApiVersion = new ApiVersion(1, 0);
				// If the client hasn't specified the API version in the request, use the default API version number 
				config.AssumeDefaultVersionWhenUnspecified = true;
				// Advertise the API versions supported for the particular endpoint
				config.ReportApiVersions = true;
			});
		}


		// API explorer version
		public static void AddVersionedApiExplorerExtension(this IServiceCollection services)
		{
			services.AddVersionedApiExplorer(o =>
			{
				o.GroupNameFormat = "'v'VVV";
				o.SubstituteApiVersionInUrl = true;
			});
		}


		//API Security
		public static void AddJWTAuthentication(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddAuthentication(auth =>
			{
				auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(token =>
			{
				token.RequireHttpsMetadata = false;
				token.SaveToken = true;
				token.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetSection("JWTSettings:Key").Value)),
					ValidateIssuer = false,
					ValidateAudience = false,
				};
			});
		}
		public static void AddGlobalAuthorization(this IServiceCollection services)
		{
			services.AddControllers(options =>
			{
				//Apply authorize attribute globally
				var policy = new AuthorizationPolicyBuilder()
					.AddAuthenticationSchemes("Bearer")
					.RequireAuthenticatedUser()
					.Build();
				options.Filters.Add(new AuthorizeFilter(policy));
			});
		}

		public static void AddAuthorizationPolicies(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddAuthorization(options =>
			{
				options.AddPolicy("AddingFiles", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "AddFiles"); });
				options.AddPolicy("DeletingFiles", policy => { policy.AddAuthenticationSchemes("Bearer"); policy.RequireClaim("AppPermissions", "DeleteFiles"); });
			});
		}

		//API Identity
		//public static void AddIdentity(this IServiceCollection services)
		//{
		//	// ===== Add Identity ========
		//	services.AddIdentity<Admin, Role>(options =>
		//	{
		//		options.Password.RequiredLength = 1;
		//		options.Password.RequireNonAlphanumeric = false;
		//		options.Password.RequireDigit = false;
		//		options.Password.RequireUppercase = false;
		//		options.Password.RequireLowercase = false;
		//	}).AddEntityFrameworkStores<AppDbContext>();
		//}

		static string XmlCommentsFilePath
		{
			get
			{
				var basePath = PlatformServices.Default.Application.ApplicationBasePath;
				var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
				return Path.Combine(basePath, fileName);
			}
		}

	}

}
