﻿using Presentation.WebApi.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Presentation.WebApi.Extensions
{
    public static class AppExtensions
    {
        public static void UseSwaggerExtension(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
				c.SwaggerEndpoint("../swagger/v1/swagger.json", "Flying Donkey task api - v1");
            });
        }

        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }
    }
}