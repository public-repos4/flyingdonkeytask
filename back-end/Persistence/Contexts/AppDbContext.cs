﻿using Core.Application.Interfaces;
using Core.Domain.Common;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Contexts
{
	//public class AppDbContext : IdentityDbContext
	public class AppDbContext : DbContext
	{
		private readonly IDateTimeService _dateTime;
		private readonly ILoggerFactory _loggerFactory;
		protected readonly IHttpContextAccessor _httpContextAccessor;

		public AppDbContext(DbContextOptions<AppDbContext> options,
		  IDateTimeService dateTime,
		  ILoggerFactory loggerFactory,
		  IHttpContextAccessor httpContextAccessor = null
		  ) : base(options)
		{
			ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking; // Must be commented on seeding the database
			_dateTime = dateTime;
			_loggerFactory = loggerFactory;
			_httpContextAccessor = httpContextAccessor;
		}

		public DbSet<File> Files { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			#region Entities

			#endregion

			#region Views

			#endregion
		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		{
			foreach (var entry in ChangeTracker.Entries<AuditableBaseEntity>())
			{
				switch (entry.State)
				{
					case EntityState.Added:
						entry.Entity.CreatedAt = _dateTime.NowUtc;
						break;

					case EntityState.Modified:
						entry.Entity.LastModifiedAt = _dateTime.NowUtc;
						break;
				}
			}
			return base.SaveChangesAsync(cancellationToken);
		}

		//public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		//{
		//	var entries = ChangeTracker
		//		.Entries()
		//		.Where(e => e.Entity is AuditableBaseEntity && (e.State == EntityState.Added || e.State == EntityState.Modified));

		//	int userId = int.Parse(_httpContextAccessor.HttpContext.Admin.FindFirst(ClaimTypes.NameIdentifier).Value);

		//	foreach (var entityEntry in entries)
		//	{
		//		if (entityEntry.State == EntityState.Added)
		//		{
		//			((AuditableBaseEntity)entityEntry.Entity).CreatedAt = DateTime.UtcNow;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedAt = DateTime.UtcNow;

		//			((AuditableBaseEntity)entityEntry.Entity).CreatedBy = userId;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedBy = userId;
		//		}

		//		if (entityEntry.State == EntityState.Modified)
		//		{
		//			entityEntry.Property("CreatedAt").IsModified = false;
		//			entityEntry.Property("CreatedBy").IsModified = false;

		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedAt = DateTime.UtcNow;
		//			((AuditableBaseEntity)entityEntry.Entity).LastModifiedBy = userId;
		//		}
		//	}

		//	return base.SaveChangesAsync(cancellationToken);
		//}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			//optionsBuilder.EnableSensitiveDataLogging();
			optionsBuilder.UseLoggerFactory(_loggerFactory);
			//optionsBuilder.UseLazyLoadingProxies();
		}
	}
}