﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Contexts
{
	public class SeedingDatabase
	{
		public async static Task Seed(IServiceProvider serviceProvider)
		{
			var appDbContext = serviceProvider.GetRequiredService<AppDbContext>();

			try
			{
				appDbContext.Database.OpenConnection();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				appDbContext.Database.CloseConnection();
			}
		}
	}
}
