﻿using Core.Application.Features.Files.Queries.GetFiles;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Core.Application.Parameters;
using Core.Domain.Entities;
using Infrastructure.Persistence.Contexts;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repositories
{
	public class FileRepositoryAsync : GenericRepositoryAsync<File>, IFileRepositoryAsync
	{
		private readonly DbSet<File> _files;

		public FileRepositoryAsync(AppDbContext dbContext) : base(dbContext)
		{
			_files = dbContext.Set<File>();
		}

		public async Task<(IEnumerable<File> data, RecordsCount recordsCount)> GetPagedFileResponseAsync(GetFilesQuery queryParams)
		{
			var orderBy = queryParams.OrderBy;
			int recordsTotal, recordsFiltered;

			// Setup IQueryable
			var query = _files
				.AsNoTracking()
				.AsExpandable();

			// Count records total
			recordsTotal = await query.CountAsync();

			// filter data
			query = FilterData(query, queryParams);

			// Count records after filter
			recordsFiltered = await query.CountAsync();

			//set Record counts
			var recordsCount = new RecordsCount
			{
				Filtered = recordsFiltered,
				Total = recordsTotal
			};

			// set order by
			if (!string.IsNullOrWhiteSpace(orderBy))
			{
				query = query.OrderBy(orderBy);
			}

			// paginate data
			query = PaginateData(query, queryParams.PageNumber, queryParams.PageSize);

			// retrieve data to list
			var resultData = await query.ToListAsync();

			// shape data
			//var shapeData = _dataShaper.ShapeData(resultData, fields);

			return (resultData, recordsCount);
		}

		private static IQueryable<File> FilterData(IQueryable<File> files, GetFilesQuery queryParams)
		{
			var id = queryParams.Id;
			var ids = queryParams.Ids;
			var search = queryParams.Search?.Trim();

			var predicate = PredicateBuilder.New<File>(true);

			if (!string.IsNullOrEmpty(id.ToString()))
			{
				predicate = predicate.Or(entity => entity.Id == id);
				return files.Where(predicate);
			}

			if (ids != null)
			{
				predicate = predicate.Or(entity => ids.Contains(entity.Id));
				return files.Where(predicate);
			}

			files = files.Where(predicate);

			return files;
		}
	}
}