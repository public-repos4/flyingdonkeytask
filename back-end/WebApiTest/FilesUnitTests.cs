using Core.Application.Features.Files.Commands.CreateFile;
using Core.Application.Features.Files.Commands.DeleteFileById;
using Core.Application.Interfaces;
using Core.Application.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Presentation.WebApi;
using System;
using System.IO;
using System.Threading.Tasks;
using static Core.Application.Features.Files.Commands.DeleteFileById.DeleteFileByIdCommand;

namespace WebApiTest
{
	[TestClass]
	public class FilesUnitTests : ControllerBase
	{
		private IFileRepositoryAsync _fileRepository;
		private IFileService _fileService;
		private IConfiguration _config;
		readonly IServiceProvider _services = Program.CreateHostBuilder(new string[] { }).Build().Services; // one liner

		[TestInitialize]
		public void Setup()
		{
			_fileRepository = _services.GetService<IFileRepositoryAsync>();
			_fileService = _services.GetService<IFileService>();
			_config = _services.GetService<IConfiguration>();
		}

		[TestMethod]
		public async Task PostFile_VaildFile_Succeeded()
		{
			//Arrange
			//Setup mock file using a memory stream
			var content = "Hello World from a Fake File";
			var fileName = "test.pdf";
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(content);
			writer.Flush();
			stream.Position = 0;

			//	//create FormFile with desired data
			IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);

			CreateFileCommand createFileByIdCommand = new() { File = file };
			CreateFileCommandHandler createFileByIdCommandHandler = new(_fileRepository, _fileService, _config);

			//Act
			var response = await createFileByIdCommandHandler.Handle(createFileByIdCommand, new System.Threading.CancellationToken());

			//Assert
			Assert.IsTrue(response.Succeeded);
		}

		[TestMethod]
		public async Task DeleteFile_VaildId_Succeeded()
		{
			//Arrange
			DeleteFileByIdCommand deleteFileByIdCommand = new() { Id = 12 };
			DeleteFileByIdCommandHandler deleteFileByIdCommandHandler = new(_fileRepository, _fileService);

			//Act
			var response = await deleteFileByIdCommandHandler.Handle(deleteFileByIdCommand, new System.Threading.CancellationToken());

			//Assert
			Assert.IsTrue(response.Succeeded);
		}
	}
}
