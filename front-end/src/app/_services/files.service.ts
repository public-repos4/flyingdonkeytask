﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';

import {map} from 'rxjs/operators';
import {ApiResponse} from '../_models/api-response';

@Injectable({providedIn: 'root'})

export class FilesService {
  private fileSubject: BehaviorSubject<File>;
  public file: Observable<File>;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
    this.fileSubject = new BehaviorSubject<File>(JSON.parse(localStorage.getItem('file')));
    this.file = this.fileSubject.asObservable();
  }

  add(file: FormData) {
    return this.http.post(`${environment.apiUrl}/files`, file);
  }

  getAll(queryParams?: HttpParams) {
    return this.http.get<ApiResponse>(`${environment.apiUrl}/files`, {params: queryParams})
      .pipe(map(response => response));
  }

  getById(id: number) {
    return this.getAll(new HttpParams({fromObject: {id: id}}))
      .pipe(map(response => response.data[0]));
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/files/${id}`).pipe();
  }
}
