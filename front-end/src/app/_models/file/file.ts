﻿export class File {
  id: number;
  originName: string;
  path: string;
  name: string;
  extension: string;
  size: string;
  uploadDate: string;
  link: string;
  isDeleting: boolean;
}
