import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {FilesRoutingModule} from './files-routing.module';
import {LayoutComponent} from './layout.component';
import {ListComponent} from './list.component';
import {UploadFileComponent} from './upload-file/upload-file.component';
import {RxReactiveFormsModule} from '@rxweb/reactive-form-validators';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    FilesRoutingModule
  ],
  declarations: [
    LayoutComponent,
    ListComponent,
    UploadFileComponent
  ]
})

export class FilesModule {
}
