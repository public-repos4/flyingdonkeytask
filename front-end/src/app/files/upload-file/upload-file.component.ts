﻿import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AlertService} from '../../_services/alert.service';
import {FilesService} from '../../_services/files.service';
import {RxwebValidators} from '@rxweb/reactive-form-validators';

@Component({templateUrl: 'upload-file.component.html'})
export class UploadFileComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private filesService: FilesService,
  ) {
  }

  uploadFileForm: FormGroup;
  loading = false;
  submitted = false;
  file: File = null;

  ngOnInit(): void {
    this.uploadFileForm = this.formBuilder.group({
      file: ['', [
        RxwebValidators.required(),
        RxwebValidators.extension({extensions: ['png', 'gif', 'jpeg', 'jpg', 'txt', 'pdf']}),
        RxwebValidators.fileSize({maxSize: 2 * 1024 * 1024}) // 2 MB
      ]]
    });
  }

  // convenience getter for easy access to form fields
  get fileForm(): { [key: string]: AbstractControl } {
    return this.uploadFileForm.controls;
  }

  onFileChange($event) {
    this.uploadFileForm.controls['file'].setValue($event.target.files.length > 0 ? $event.target.files[0] : null);
  }

  onSubmit(): void {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.uploadFileForm.invalid) {
      return;
    }

    this.loading = true;
    this.addFile();
  }

  private addFile() {
    console.log(this.uploadFileForm.value);
    // const formData: any = new FormData(this.uploadFileForm.value);
    const formData: any = new FormData();
    formData.append('file', this.uploadFileForm.controls.file.value);

    this.filesService.add(formData)
      .pipe(first())
      .subscribe({
        next: () => {
          this.alertService.success('SucceededAdd', {keepAfterRouteChange: true, autoClose: true});
          this.router.navigate(['/files']);
        },
        error: error => {
          this.alertService.error(error);
          this.loading = false;
        }
      });
  }
}
