﻿import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {File} from '../_models/file/file';
import {FilesService} from '../_services/files.service';
import {AlertService} from '../_services/alert.service';

@Component({templateUrl: 'list.component.html'})
export class ListComponent implements OnInit {
  pngFiles: File[];
  gifFiles: File[];
  jpegFiles: File[];
  jpgFiles: File[];
  textFiles: File[];
  pdfFiles: File[];

  constructor(
    private alertService: AlertService,
    private filesService: FilesService,
  ) {
  }

  ngOnInit(): void {
    this.filesService.getAll()
      .pipe(first())
      .subscribe(response => {
        this.pngFiles = response.data.filter(item => item.extension.toLowerCase() === '.png').map(item => ({...item, isDeleting: false}));
        this.gifFiles = response.data.filter(item => item.extension.toLowerCase() === '.gif').map(item => ({...item, isDeleting: false}));
        this.jpegFiles = response.data.filter(item => item.extension.toLowerCase() === '.jpeg').map(item => ({...item, isDeleting: false}));
        this.jpgFiles = response.data.filter(item => item.extension.toLowerCase() === '.jpg').map(item => ({...item, isDeleting: false}));
        this.textFiles = response.data.filter(item => item.extension.toLowerCase() === '.txt').map(item => ({...item, isDeleting: false}));
        this.pdfFiles = response.data.filter(item => item.extension.toLowerCase() === '.pdf').map(item => ({...item, isDeleting: false}));
      });
  }

  deleteFile(id: number, extension: string): void {
    let files;
    switch (extension) {
      case '.png':
        files = this.pngFiles;
        break;
      case '.gif':
        files = this.gifFiles;
        break;
      case '.jpeg':
        files = this.jpegFiles;
        break;
      case '.jpg':
        files = this.jpgFiles;
        break;
      case '.txt':
        files = this.textFiles;
        break;
      case '.pdf':
        files = this.pdfFiles;
        break;
    }
    files.find(x => x.id === id).isDeleting = true;
    this.filesService.delete(id)
      .pipe(first())
      .subscribe(() => {
        this.alertService.success('SucceededDelete', {keepAfterRouteChange: true, autoClose: true});
        files = files.filter(item => item.id !== id);
        switch (extension) {
          case '.png':
            this.pngFiles = this.pngFiles.filter(item => item.id !== id);
            break;
          case '.gif':
            this.gifFiles = this.gifFiles.filter(item => item.id !== id);
            break;
          case '.jpeg':
            this.jpegFiles = this.jpegFiles.filter(item => item.id !== id);
            break;
          case '.jpg':
            this.jpgFiles = this.jpgFiles.filter(item => item.id !== id);
            break;
          case '.txt':
            this.textFiles = this.textFiles.filter(item => item.id !== id);
            break;
          case '.pdf':
            this.pdfFiles = this.pdfFiles.filter(item => item.id !== id);
            break;
        }
      });
  }
}

