import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from '@app/home';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'files', loadChildren: () => import('./files/files.module').then(x => x.FilesModule)},

  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
